<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model'=>$model,
    'attributes'=>[
        'title',
        'preview',
        [
            'label'=>'Badge',
            'value'=>  Html::img($model->urlAttribute('badge')),
            'format'=>'html'
        ]
    ]
]);

