<?php 
use yii\grid\GridView;

?>
<div class="publications-main-index">
    <h1><?=Yii::t('publications','Publications')?></h1>
    <p>
        Страница находится в разработке
    </p>
    <?=GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns'=>[
            'id',
            'preview'
        ]
    ])?>
</div>
