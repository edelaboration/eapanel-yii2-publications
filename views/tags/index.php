<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use eapanel\publications\Module;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('publications', 'Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-index">
    <div class="panel panel-default">
        <div class="panel-heading">Добавление нового тега</div>
        <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>
    
    <?php Pjax::begin(['id' => 'view-index']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'=>$searchModel,
            'columns' => [
                'id',
                'name',
                'body',
                [
                    'attribute'=>'type',
                    'filter'=>  eapanel\publications\models\Tag::getTypes(),
                    'value'=>function($model){return $model->typeName;}
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end()?>

</div>
