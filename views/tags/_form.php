<?php

use yii\helpers\Html;
use eapanel\publications\Module;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model eapanel\publications\models\Tag */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$("document").ready(function(){
        $("#new-record").on("pjax:end", function() {
        $.pjax.reload({container:"#view-index"});  //Reload GridView
    });
});'
);
?>

<div class="tag-form">
<?php Pjax::begin(['id'=>'new-record']);?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <div class="row">
        <div class="col-sm-3"> <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>

        <div class="col-sm-3"><?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-3">
            <?=$form->field($model, 'type')->dropDownList(eapanel\publications\models\Tag::getTypes())?>
        </div>
        <div class="col-sm-3">
            <br>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('publications', 'Add a new tag') : Yii::t('app_labels', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
<?php Pjax::end()?>
</div>
