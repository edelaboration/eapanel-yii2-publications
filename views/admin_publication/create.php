<?php

/* @var $this yii\web\View */
/* @var $model app\modules\publications\models\PublicationBase */

$this->title = Yii::t('publications', "Create {$model->typeName}");
$this->params['breadcrumbs'][] = ['label' => Yii::t('publications', "{$model->typeName}s"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="publication-create">
    <?= $this->render($formView, [
        'model' => $model,
    ]) ?>
</div>
