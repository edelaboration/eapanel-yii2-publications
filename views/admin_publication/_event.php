<?php

use yii\helpers\Html;
use eapanel\publications\Module;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as Wysiwig;
use vova07\fileapi\Widget as FileAPI;
use kartik\datetime\DateTimePicker as DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\publications\models\PublicationBase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publication-base-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>false,'enableClientScript'=>false]); ?>
    <div class="row">
        <div class="col-lg-5">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($model, 'preview')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'badge')->textInput(['maxlength' => 128])->widget(FileAPI::className(),[
                'settings' => [
                    'url' => ['/publications/main/fileapi-upload']
                ],
                'crop'=>$this->context->module->crop,
                'jcropSettings'=>$this->context->module->jcropSettings,
                'cropResizeWidth'=>$this->context->module->cropResizeWidth,
                'cropResizeHeight'=>$this->context->module->cropResizeHeight
            ]) ?>
        </div>
        
        <div class="col-lg-7">
            <?= $form->field($model, 'created_in')->widget(DatePicker::className(),[
                'pluginOptions' => [
                    'autoclose' => true,
                    'format'=>'dd-mm-yyyy hh:ii',
                    'startDate'=> date('Y-m-d')
                ]
            ])?>
            <?= $form->field($model, 'body')->textarea(['rows' => 6])->widget(Wysiwig::className(),[
                'settings'=>[
                    'lang'=>'ru',
                    'minHeight' => 300,
                    'imageUpload' => Url::to(["/{$this->context->module->id}/main/image-upload"]),
                    'imageManagerJson' => Url::to(["/{$this->context->module->id}/main/images-get"]),
                    'fileUpload' => Url::to(['/pages/main/file-upload']),
                    'fileManagerJson' => Url::to(['/pages/main/files-get']),
                    'plugins' => [
                        'clips',
                        'fullscreen',
                        'imagemanager',
                        'filemanager',
                        'table',
                        'video',
                        'fontsize',
                    ],
                    'buttons' => ['html', 'formatting', 'bold', 'italic', 'deleted',
                        'unorderedlist', 'orderedlist', 'outdent', 'indent',
                        'image', 'file', 'link', 'alignment', 'horizontalrule'],
                    'buttonSource' => true,
                    'replaceDivs'=>false
                ]
            ]) ?>   
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('publications', 'Create') : Yii::t('publications', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
