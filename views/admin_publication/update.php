<?php

/* @var $this yii\web\View */
/* @var $model app\modules\publications\models\PublicationBase */

$this->title = Yii::t('publications', "Update {$model->typeName}: ") . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('publications', "{$model->typeName}s"), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = "{$model->title}";
?>

<div class="publication-base-update">

    <?= $this->render($formView, [
        'model' => $model,
    ]) ?>

</div>
