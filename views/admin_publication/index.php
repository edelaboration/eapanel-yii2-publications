<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel eapanel\publications\models\PublicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('publications',$nameOfPublicationType.'s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publications-index">

<p>
    <?= Html::a(Yii::t('publications', "Create $nameOfPublicationType"), ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?php \yii\widgets\Pjax::begin(['enablePushState'=>false]); ?>
    <?= GridView::widget([
        'layout'=>"{pager}{items}\n{summary}{pager}",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            ['attribute'=>'created_in','format'=>['date','php:d-M-Y H:i']],
            ['attribute'=>'updated_in','format'=>['date','php:d-M-Y H:i']],
            'author_id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php yii\widgets\Pjax::end();?>
</div>
