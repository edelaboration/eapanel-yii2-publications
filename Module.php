<?php

namespace eapanel\publications;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class Module extends \yii\base\Module
{
    public $defaultRoute = 'main';
    
    public $imageUploadPath = '/images/publications';
    
    public $imageManagerPath = '/images/publications';

    public $adminAccessRuleName = 'Administrator';

    public $controllerNamespace = 'eapanel\publications\controllers';

    public $crop = true;
    
    public $cropResizeWidth;
    
    public $cropResizeHeight;
    
    public $jcropSettings=[];

    private $_defaultJcropSettings = [
        'aspectRatio' => 1,
        'bgColor' => '#ffffff',
        'maxSize' => [850, 550],
        'minSize' => [100, 100],
        'keySupport' => false, // Important param to hide jCrop radio button.
        'selection' => '100%'
    ];
    
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'controllers'=>["{$this->id}/main","{$this->id}/news","{$this->id}/articles","{$this->id}/hotels"]
                    ],
                    [
                        'allow' => true,
                        'roles' => [$this->adminAccessRuleName],
                        'controllers'=>[ 
                            "{$this->id}/adminnews",
                            "{$this->id}/adminarticle",
                            "{$this->id}/adminevent",
                            "{$this->id}/adminhotel",
                            "{$this->id}/tags",
                        ]
                    ]
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
        $this->jcropSettings = array_merge($this->_defaultJcropSettings, $this->jcropSettings);
    }
}
