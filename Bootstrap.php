<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications;

/**
 * Description of Bootstrap
 *
 * @author Tartharia
 */
class Bootstrap implements \yii\base\BootstrapInterface{
    
    /** @var array Model's map */
    /*private $_modelMap = [
        'Article'      => 'eapanel\publications\models\User',
        'News'         => 'eapanel\publications\models\Account',
        'Event'        => 'eapanel\publications\models\Profile',
        'Hotel'        => 'eapanel\publications\models\Token',
        'Tag'          => 'eapanel\publications\RegistrationForm',
        'Rubric'       => 'eapanel\publications\models\ResendForm',
    ];*/
    
    /**
     * 
     * @param \yii\base\Application $app
     */
    public function bootstrap($app) {
        if($app instanceof \yii\web\Application)
        {
            $app->urlManager->addRules([
                ['class' => 'yii\web\UrlRule' , 'pattern' => '<_c:(news|articles)>/<rubric:[\w\d\-]+>/<id:[\w\d\-]+>','route'=>'publications/<_c>/view'],
                ['class' => 'yii\web\UrlRule' , 'pattern' => '<_c:(news|articles|hotels)>/show/<id:[\w\d\-]+>','route' => 'publications/<_c>/view'],
                ['class' => 'yii\web\UrlRule' , 'pattern' => '<_c:(news|articles)>/<rubric:[\w\d\-]+>','route'=>'publications/<_c>/index'],
                ['class' => 'yii\web\UrlRule' , 'pattern' => '<_c:(news|articles|hotels)>','route'=>'publications/<_c>/index'],
            ],false);
            
            $app->i18n->translations['publications'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@eapanel/publications/messages',
            ];
        }
    }
}
