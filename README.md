Yii2 Publications module v.2.0.1-alpha
===============================

Добавлена возможность сохранять ЧПУ для публикаций.

# Overriding models

Assume you decided to override Article class. Let’s create new Article class under your namespace. For example`@app/models`.

```php
namespace app\models;

class Article extends eapanel\publications\models\Article
{
    public function init()
    {
        // custom code
        parent::init();
    }
}
```

In order to make yii2-publications use your class you need to configure [Dependency Injection container] in index.php file as follows:

```php
...
Yii::$container->set(\eapanel\publications\models\Article::className(), \app\models\Article::className());

(new yii\web\Application($config))->run();
...
```