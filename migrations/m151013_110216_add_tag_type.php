<?php

use eapanel\publications\migrations\Migration;

class m151013_110216_add_tag_type extends Migration
{
    public $table = '{{%tag}}';
    public function up()
    {
        $this->addColumn($this->table, 'type', "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Tag type'");
    }

    public function down()
    {
        $this->dropColumn($this->table, 'type');
    }
}
