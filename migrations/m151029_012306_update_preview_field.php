<?php

use yii\db\Migration;

class m151029_012306_update_preview_field extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%publication}}", 'preview', " MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Текст для превью'");
    }

    public function down()
    {
        echo "m151029_012306_update_preview_field cannot be reverted.\n";
    }
}
