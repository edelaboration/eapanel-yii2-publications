<?php

use eapanel\publications\migrations\Migration;
use eapanel\publications\models\Tag;
use yii\db\Query;


class m160916_082252_transfer_rubrics_from_tags_to_separate_table extends Migration
{
    public $tagTbl = "{{%tag}}";
    
    public $rubricTbl = "{{%rubric}}";
    
    public $publicationTbl = '{{%publication}}';
    
    public $publicationTagTbl = "{{%publication_tag}}";

    public function safeUp()
    {
        $this->createTable($this->rubricTbl,[
            'id' =>  $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
        ], $this->tableOptions);
        
        $this->batchInsert($this->rubricTbl, ['id','title','alias'],(new Query())
                ->from('{{%tag}}')
                ->select('id,body,name')
                ->where(['type' => Tag::TYPE_RUBRIC])->all()
            );
        
        $this->addColumn($this->publicationTbl,'rubric_id', $this->integer()->null()->defaultValue(null)->after('body'));
        
        $this->execute("INSERT INTO $this->publicationTbl(id,rubric_id) SELECT publication_id,tag_id FROM {{%publication_tag}} LEFT JOIN {{%tag}} tag ON tag_id=tag.id WHERE tag.type=".Tag::TYPE_RUBRIC." ON DUPLICATE KEY UPDATE rubric_id=tag_id");

        $this->createIndex('fk_publication_rubric_idx', $this->publicationTbl, 'rubric_id');
        $this->addForeignKey('fk_publication_rubric', $this->publicationTbl,'rubric_id',  $this->rubricTbl,'id','RESTRICT','CASCADE');
        
        $this->delete($this->tagTbl, ['type' => Tag::TYPE_RUBRIC]);
    }

    public function safeDown()
    {
        $this->execute("INSERT INTO {$this->tagTbl}(id,name,body,type) SELECT id,alias,title,".Tag::TYPE_RUBRIC." FROM {$this->rubricTbl}");
        $this->execute("INSERT INTO {$this->publicationTagTbl}(publication_id,tag_id) SELECT id,rubric_id FROM {$this->publicationTbl} WHERE rubric_id IS NOT NULL");
        
        $this->dropForeignKey('fk_publication_rubric', $this->publicationTbl);
        $this->dropColumn($this->publicationTbl,'rubric_id');
        $this->dropTable($this->rubricTbl);
    }
}
