<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Inflector;

class m151207_170706_create_alias_for_publication_url extends Migration
{
    public $table = '{{%publication}}';
    
    public function safeUp()
    {
        $this->execute("SET AUTOCOMMIT = 0; SET FOREIGN_KEY_CHECKS = 0; SET UNIQUE_CHECKS = 0;");
        $this->addColumn($this->table, 'alias', Schema::TYPE_STRING . "(255) NOT NULL AFTER `id`");        
        $this->addColumn($this->table, 'alias_index', "CHAR(32) NOT NULL AFTER `id`");
        
        $query = new Query;        
        $publications = $query->from($this->table)->select('id,title')->all();
        
        if(is_array($publications))
        {
            foreach ($publications as $publication) {
                $alias = Inflector::slug($publication['title'].'-'.$publication['id']);
                $this->update($this->table,[
                    'alias'=>  $alias,
                    'alias_index'=>  md5($alias)
                ],'id = :pubId', ['pubId'=>$publication['id']]);
            }
        }
        $this->createIndex('alias_idx', $this->table, 'alias_index',true);
        $this->execute("SET AUTOCOMMIT = 1; SET FOREIGN_KEY_CHECKS = 1; SET UNIQUE_CHECKS = 1;");
    }

    public function down()
    {
        $this->dropIndex('alias_idx', $this->table);
        $this->dropColumn($this->table,'alias');
        $this->dropColumn($this->table,'alias_index');
    }
}
