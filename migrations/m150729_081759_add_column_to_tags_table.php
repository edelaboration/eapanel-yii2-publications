<?php

use yii\db\Schema;
use eapanel\publications\migrations\Migration;

class m150729_081759_add_column_to_tags_table extends Migration
{
    public $table = "{{%tag}}";
    public function up()
    {
        $this->renameColumn($this->table,'name','body');
        $this->addColumn($this->table,'name',  Schema::TYPE_STRING."(128) NOT NULL COMMENT 'tag label' AFTER `id`");
        $this->createIndex('tag_label_idx', $this->table,'name');
    }

    public function down()
    {
        $this->dropColumn($this->table,'body');
        $this->dropIndex('tag_label_idx', $this->table);
        $this->alterColumn($this->table,'name', Schema::TYPE_STRING . '(255) NOT NULL');
    }
}
