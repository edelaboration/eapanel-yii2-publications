<?php

namespace eapanel\publications\migrations;

/**
 * Description of Migration
 *
 * @author Tartharia
 */
class Migration extends \yii\db\Migration{
    /**
     * @var string
     */
    protected $tableOptions;
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        switch (\Yii::$app->db->driverName) {
            case 'mysql':
                $this->tableOptions = 'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci';
                break;
            case 'pgsql':
                $this->tableOptions = null;
                break;
            default:
                throw new \RuntimeException('Your database is not supported!');
        }
    }
}
