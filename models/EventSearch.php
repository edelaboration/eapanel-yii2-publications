<?php

namespace eapanel\publications\models;

/**
 * PublicationSearch represents the model behind the search form about `app\modules\publications\models\PublicationBase`.
 */
class EventSearch extends PublicationSearch
{
    public function init() {
        $this->type = PublicationBase::TYPE_EVENT;
        parent::init();
    }
}
