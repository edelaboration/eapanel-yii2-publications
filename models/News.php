<?php

namespace eapanel\publications\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Description of NewsModel
 *
 * @author Tartharia
 */
class News extends PublicationBase{
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        if(isset($behaviors['seotags']))
        {
            $behaviors['seotags']['viewRoute'] = "publications/news/view";
        }
        return $behaviors;
    }

    public function init() {
        $this->type = self::TYPE_NEWS;
        parent::init();
    }

    public static function tableName()
    {
        return '{{%publication}}';
    }
    
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'title'=>  Yii::t('publications', 'News title')
        ]);
    }
    
    public static function getTypeName() {
        return 'News';
    }
    
    public static function find() {
        return parent::find()->andFilterWhere(['type'=>self::TYPE_NEWS]);        
    }
}