<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\models;

/**
 * Description of TagQuery
 *
 * @author Tartharia
 */
class TagQuery extends \yii\db\ActiveQuery{
    
    /**
     * 
     * @return TagQuery
     */
    public function toponyms()
    {
        return $this->where([Tag::tableName().'.type' => Tag::TYPE_REGION]);
    }
    
    /**
     * 
     * @return TagQuery
     */
    public function categories()
    {
        return $this->where([Tag::tableName().'.type' => Tag::TYPE_CATEGORY]);
    }

    /**
     * @inheritdoc
     * @return Tag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
