<?php

namespace eapanel\publications\models;

use Yii;

/**
 * This is the model class for table "{{%publication_tag}}".
 *
 * @property integer $publication_id
 * @property integer $tag_id
 *
 * @property Publication $publication
 * @property Tag $tag
 */
class PublicationTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%publication_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publication_id', 'tag_id'], 'required'],
            [['publication_id', 'tag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'publication_id' => Yii::t('app', 'Publication ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublication()
    {
        return $this->hasOne(Publication::className(), ['id' => 'publication_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
}
