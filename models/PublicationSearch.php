<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\models;

use yii\data\ActiveDataProvider;
use eapanel\publications\models\PublicationBase;

/**
 * Description of PublicationSearch
 *
 * @author Tartharia
 * @property Rubric $rubric
 */
class PublicationSearch extends \yii\base\Model{

    public $id;

    public $title;

    public $type;

    public $preview;

    public $body;

    public $rubric_id;

    public $created_in;

    public $updated_in;

    /**
     *
     * @var Rubric
     */
    private $_rubric;


    public function rules()
    {
        return [
            [['id','type','rubric_id'], 'integer'],
            [['rubric'],'safe'],
            [['created_in', 'updated_in', 'title', 'preview', 'body'], 'safe'],
        ];
    }

    /**
     *
     * @param type $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params=[],$formName = null)
    {
        if(!isset($formName))
        {
            $formName = $this->formName();
        }

        $query = PublicationBase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->getSort()->defaultOrder = ['id'=>SORT_DESC];

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_in' => $this->created_in,
            'updated_in' => $this->updated_in,
            'rubric_id' => $this->rubric_id,
            PublicationBase::tableName().'.type'=>  $this->type
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'preview', $this->preview])
            ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }

    public function setRubric($rubric)
    {
        $this->_rubric = Rubric::find()->where(['alias'=>$rubric])->one();
        $this->rubric_id = $this->_rubric?$this->_rubric->id : null;
    }

    /**
     *
     * @return Rubric
     */
    public function getRubric()
    {
        return $this->_rubric;
    }

    /**
     *
     * @deprecated since version 2.0.2
     * @param type $params
     * @return ActiveDataProvider
     */
    public function filterByTag($params)
    {
        $query = PublicationBase::find();

        $dataProvider = new ActiveDataProvider(['query'=>$query]);
        $this->load($params,'');
        $query->joinWith('tags')
                ->andFilterWhere(['tag.name'=>  $this->rubric,'type'=>  $this->type]);

        return $dataProvider;
    }
}
