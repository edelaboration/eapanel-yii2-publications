<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\models;

/**
 * Description of Hotel
 *
 * @author Tartharia
 */
class Hotel extends PublicationBase{
    
    public function init() {
        $this->type = self::TYPE_HOTEL;
        parent::init();
    }
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        if(isset($behaviors['seotags']))
        {
            $behaviors['seotags']['viewRoute'] = "publications/hotels/view";
        }
        return $behaviors;
    }
    
    public static function getTypeName() {
        return 'Hotel';
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function find() {
        return parent::find()->andFilterWhere([PublicationBase::tableName() .'.type'=>self::TYPE_HOTEL]);        
    }
}
