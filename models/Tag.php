<?php

namespace eapanel\publications\models;

use Yii;
use eapanel\publications\models\PublicationBase as Publication;

/**
 * This is the model class for table "{{%tag}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property integer $type
 *
 * @property PublicationTag[] $publicationTags
 * @property Publication[] $publications
 */
class Tag extends \yii\db\ActiveRecord
{
    const TYPE_GENERAL = 1;
    
    const TYPE_REGION = 2;
    
    const TYPE_CATEGORY = 3;
    
    const TYPE_RUBRIC = 4;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','body','type'], 'required'],
            [['name'],'string', 'max' => 128],
            [['name'],'match','pattern'=>'/^[A-Za-z\d\s\_\-]*$/u'],
            [['body'],'string', 'max' => 255],
            [['name','body'],'unique'],
            [['type'],'integer','min'=>1,'max'=>4,'message'=>Yii::t('publications', 'Unknown tag type \'{0}\'',['{value}'])],
            [['type'],'default','value'=>1]
        ];
    }
    
    /**
     * 
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_GENERAL => Yii::t('publications', 'General'),
            self::TYPE_REGION => Yii::t('publications', 'Toponym'),
            self::TYPE_CATEGORY => Yii::t('publications', 'Category'),
        ];
    }
    
    public function getTypeName(){
        return static::getTypes()[$this->type];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('publications', 'ID'),
            'name' => Yii::t('publications', 'Tag label'),
            'body' => Yii::t('publications', 'Tag body'),
            'type' => Yii::t('publications', 'Tag type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationTags()
    {
        return $this->hasMany(PublicationTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['id' => 'publication_id'])->viaTable('{{%publication_tag}}', ['tag_id' => 'id']);
    }

    /*public static function rubricItems()
    {
        $items = [];
        foreach (self::findRubrics() as $rubric) {
            $items[] = ['label'=>$rubric->body,'url'=>['/publications/main/index','rubric'=>$rubric->name]];
        }
        return $items;
    }*/

    /**
     * 
     * @return \eapanel\publications\models\TagQuery
     */
    public static function find() {
        return new TagQuery(get_called_class());
    }
}
