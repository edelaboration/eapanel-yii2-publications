<?php

namespace eapanel\publications\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Description of Events
 *
 * @author Tartharia
 */
class Event extends PublicationBase{
    
    public $start_time;
   
    
    public function init() {
        $this->type = self::TYPE_EVENT;
        parent::init();
    }

    public static function tableName()
    {
        return '{{%publication}}';
    }
    
    public static function getTypeName() {
        return 'Event';
    }
    
    public function rules() {
        return ArrayHelper::merge(parent::rules(),[
            [['created_in'],'required','on'=>['create','update']],
            [['created_in'],'date','format'=>'php:d-m-Y h:i']
        ]);
    }

    public function attributeLabels() {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'title'=>  Yii::t('publications','Event title'),
            'created_in' => Yii::t('publications', 'Date of the event'),
            'start_time' => Yii::t('publications', 'Start time'),
        ]);
    }
    
    public function beforeSave($insert) {
        if(!parent::beforeSave($insert))
            return false;
        
        $this->created_in = date('Y-m-d H:i:s',strtotime($this->created_in));   
        return true;
    }

    public static function find() {
        return parent::find()->andFilterWhere(['type'=>self::TYPE_EVENT]);        
    }
}