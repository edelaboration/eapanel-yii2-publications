<?php

namespace eapanel\publications\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Application as WebApplication;

/**
 * This is the model class for table "publication".
 *
 * @property integer $id
 * @property string $alias value which is used as a parameter in the URL
 * @property string $created_in
 * @property string $updated_in
 * @property integer $author_id
 * @property integer $type
 * @property string $title
 * @property string $badge
 * @property string $preview
 * @property string $body
 * @property-read string $typeName текстовое название типа публикации
 *
 * @property CommentOnPublication[] $commentOnPublications
 * @property User $author
 * @property PublicationTag[] $publicationTags
 * @property Tag[] $tags
 * @property Rubric $rubric
 * 
 * @method \yii\db\ActiveQuery findByIndexed(string $param) Description
 */

class PublicationBase extends ActiveRecord
{
    const TYPE_NEWS = 1;
    const TYPE_ARTICLE = 2;
    const TYPE_EVENT = 3;
    const TYPE_HOTEL = 4;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%publication}}';
    }
    
    public function init() {
        parent::init();
        if($this->typeName == 'base' && !$this instanceof PublicationBase)
        {
            throw new \yii\base\InvalidConfigException('Геттер getTypeName() должен быть определен в классе потомке. See [PublicationBase::typeName].');
        }
    }

    public function behaviors() {
        $behaviors = [
            'indexed'=>[
                'class'=> 'eapanel\publications\components\IndexedStringBehavior',
                'attribute'=>'alias',
                'indexAttribute'=>'alias_index'
            ],
            'many-to-many'=>[
                'class'=>  'voskobovich\behaviors\ManyToManyBehavior',
                'relations'=>[
                    'tags_ids'=>'tags'
                ]
            ]
        ];
        
        if(Yii::$app instanceof WebApplication)
        {
            $behaviors = ArrayHelper::merge($behaviors,[
                'uploadBehavior' => [
                    'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                    'attributes' => [
                        'badge' => [
                            'path' => '@webroot/images/publications',
                            'tempPath' => '@app/runtime',
                            'url' => '/images/publications'
                        ]
                    ]
                ],
                'seotags' =>[
                    'class'=>  \robote13\SEOTags\components\MetaTagsBehavior::className()
                ]
            ]);
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body','title','alias'], 'required'],
            [['preview', 'body'], 'string'],
            [['title','alias'], 'string', 'max' => 255],
            [['alias'],'match','pattern'=>'/^[A-Za-z\d\-]{3,}$/','skipOnEmpty'=>false,'message'=>  Yii::t('publications', "Only latin characters, numbers and hyphens are allowed")],
            [['badge'], 'string', 'max' => 128],
            [['tags_ids'],'each','rule'=>['exist','targetClass'=>  Tag::className(),'targetAttribute'=>'id']],
            [['rubric_id'],'exist','targetClass'=>  Rubric::className(),'targetAttribute'=>'id']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('publications', 'ID'),
            'created_in' => Yii::t('publications', 'Создано'),
            'updated_in' => Yii::t('publications', 'Обновлено'),
            'author_id' => Yii::t('publications', 'ИД автора публикации'),
            'type' => Yii::t('publications', 'Тип публикации'),
            'title' => Yii::t('publications', 'Publication title'),
            'badge' => Yii::t('publications', 'Картинка для превью'),
            'preview' => Yii::t('publications', 'Текст для превью'),
            'body' => Yii::t('publications', 'Текст'),
            'tags' => Yii::t('publications', 'Tags'),
            'alias' => Yii::t('publications','URL slug'),
            'alias_index' => Yii::t('publications','URL slug'),
            'rubric_id' => Yii::t('publications','Rubric')
        ];
    }

    public function beforeSave($insert) {
        if(!parent::beforeSave($insert))
        {
            return false;
        }
        
        if($insert)
        {
            if(!isset($this->author_id))
            {
                $this->author_id = Yii::$app->user->id;
            }
        }
        else{
            $this->updated_in = date('Y-m-d H:i:s');
        }
        
        return true;
    }

    public static function typeNames()
    {
        return[
            self::TYPE_NEWS=>'News',
            self::TYPE_ARTICLE=>'Article',
            self::TYPE_EVENT=>'Event',
            self::TYPE_HOTEL => 'Hotel'
        ];
    }
    
    /**
     * Getter for typeName.
     * @return string the textual name of the publication type
     */
    public static function getTypeName()
    {
        return 'base';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentOnPublications()
    {
        return $this->hasMany(CommentOnPublication::className(), ['publication_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(\Yii::$app->getModule('user')->modelMap['User'], ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationTags()
    {
        return $this->hasMany(PublicationTag::className(), ['publication_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->via('publicationTags');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id']);
    }
    
    public function getToponyms()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->from('{{%tag}} tag_t')
                ->viaTable('{{%publication_tag}} top', ['publication_id' => 'id'])->onCondition('tag_t.type='.Tag::TYPE_REGION);
    }
    
    public static function instantiate($row) {
        if(get_class() == PublicationBase::className())
        {
            switch ($row['type']){
                case self::TYPE_NEWS:
                    return Yii::createObject(News::className());
                case self::TYPE_ARTICLE:
                    return Yii::createObject(Article::className());
                case self::TYPE_EVENT:
                    return Yii::createObject(Event::className());
                case self::TYPE_HOTEL:
                    return Yii::createObject(Hotel::className());
                default :
                    return new static;
            }
        }
        return new static;        
    }
}
