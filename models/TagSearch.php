<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace eapanel\publications\models;

use eapanel\publications\models\Tag;
/**
 * Search Tags
 *
 * @author Tartharia
 */

class TagSearch extends \yii\base\Model{
    
    /**
     * @var string Tag alias {@see eapanel\publications\models\Tag} 
     */
    public $name;
    
    /**
     *
     * @var string Tag body {@see eapanel\publications\models\Tag}
     */
    public $body;
    
    /**
     *
     * @var string Tag type {@see eapanel\publications\models\Tag}
     */
    public $type;
    
    public function rules() {
        return[
            [['name','body'],'safe'],
            [['type'],'integer']
        ];
    }

    public function search($params,$formName = null)
    {
        $query = Tag::find();
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query'=>$query
        ]);
        
        $this->load($params,$formName);
        
        $query->andFilterWhere(['like','name',  $this->name]);
        $query->andFilterWhere(['like','body',  $this->body]);
        $query->andFilterWhere(['type'=>  $this->type]);
        
        return $dataProvider;
    }
}