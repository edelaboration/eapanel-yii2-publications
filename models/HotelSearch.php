<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\models;

/**
 * Description of HotelSearch
 *
 * @author Tartharia
 */
class HotelSearch extends PublicationSearch{
    public function init() {
        $this->type = PublicationBase::TYPE_HOTEL;
        parent::init();
    }
}
