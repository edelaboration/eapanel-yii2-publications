<?php

namespace eapanel\publications\models;


/**
 * PublicationSearch represents the model behind the search form about `app\modules\publications\models\PublicationBase`.
 */
class NewsSearch extends PublicationSearch
{
    public function init() {
        $this->type = PublicationBase::TYPE_NEWS;
        parent::init();
    }
}
