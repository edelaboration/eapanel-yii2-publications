<?php

namespace eapanel\publications\models;

use Yii;

/**
 * This is the model class for table "{{%rubric}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 *
 * @property Publication[] $publications
 */
class Rubric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rubric}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['title', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('publications', 'ID'),
            'title' => Yii::t('publications', 'Title'),
            'alias' => Yii::t('publications', 'Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(PublicationBase::className(), ['rubric_id' => 'id']);
    }
}
