<?php

namespace eapanel\publications\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Description of NewsModel
 *
 * @author Tartharia
 */
class Article extends PublicationBase{
    
    public static function tableName()
    {
        return '{{%publication}}';
    }
    
    public function init() {
        $this->type = self::TYPE_ARTICLE;
        parent::init();
    }
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        if(isset($behaviors['seotags']))
        {
            $behaviors['seotags']['viewRoute'] = "publications/articles/view";
        }
        return $behaviors;
    }
    
    public function rules() {
        return ArrayHelper::merge(parent::rules(),[
            ['rubric_id','required']
        ]);
    }


    public function attributeLabels() {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'title'=>  Yii::t('publications','Article title')
        ]);
    }
    
    public static function getTypeName() {
        return 'Article';
    }

    public static function find() {
        return parent::find()->andFilterWhere([PublicationBase::tableName() .'.type'=>self::TYPE_ARTICLE]);        
    }
}