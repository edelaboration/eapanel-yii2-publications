<?php

namespace eapanel\publications\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use eapanel\publications\models\PublicationSearch;
use eapanel\publications\models\PublicationBase;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use vova07\imperavi\actions\UploadAction as WysywigUploadAction;

/**
 * @deprecated since version 2.0.2
 */
class MainController extends Controller
{
    public $layout = '/main';
    public function actions() {
        return[
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@app/runtime'
            ],
            'image-upload' => [
                'class' => WysywigUploadAction::className(),
                'url' => $this->module->imageUploadPath,
                'path' => "@webroot/{$this->module->imageUploadPath}"
            ],
            'images-get' => [
                'class' => \vova07\imperavi\actions\GetAction::className(),
                'url' => $this->module->imageManagerPath,
                'path' => "@webroot/{$this->module->imageManagerPath}",
                'type' => \vova07\imperavi\actions\GetAction::TYPE_IMAGES,
            ],
        ];
    }

    public function actionIndex()
    {
        /* @var $dataProvider \yii\data\ActiveDataProvider */
        $model = new PublicationSearch();
        $dataProvider = $model->filterByTag(\Yii::$app->request->getQueryParams());
        $dataProvider->pagination = ['pageSize'=>20];
        $dataProvider->query->orderBy('id DESC');
        return $this->render('index',['dataProvider'=>$dataProvider]);
    }
    
    public function actionView($id)
    {
        return $this->render('view',['model'=>  $this->findModel($id)]);
    }
    
    protected function findModel($id) {
        $model = new PublicationBase();
        if (($model = $model->findByIndexed($id)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }    
    }
}
