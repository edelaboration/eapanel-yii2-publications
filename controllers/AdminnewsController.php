<?php

namespace eapanel\publications\controllers;

class AdminnewsController extends AdminPublicationControllerAbstract
{    
    public function getModelClass() {
        return 'eapanel\publications\models\News';
    }
    
    public function getSearchClass() {
        return 'eapanel\publications\models\NewsSearch';
    }

}
