<?php

namespace eapanel\publications\controllers;

class AdminarticleController extends AdminPublicationControllerAbstract
{
    public function getModelClass() {
        return 'eapanel\publications\models\Article';
    }
    
    public function getSearchClass(){
        return 'eapanel\publications\models\ArticleSearch';
    }
}