<?php

namespace eapanel\publications\controllers;

class AdmineventController extends AdminPublicationControllerAbstract
{    
    public $formView = '_event';

    public function getModelClass() {
        return 'eapanel\publications\models\Event';
    }
    
    public function getSearchClass() {
        return 'eapanel\publications\models\EventSearch';
    }
}
