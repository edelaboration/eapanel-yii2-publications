<?php

namespace eapanel\publications\controllers;

class NewsController extends \eapanel\publications\controllers\FrontendControllerAbstract
{
    public function getModelClass() {
        return 'eapanel\publications\models\News';
    }
    
    public function getSearchClass() {
        return 'eapanel\publications\models\NewsSearch';
    }

}
