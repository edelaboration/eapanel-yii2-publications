<?php

namespace eapanel\publications\controllers;

class ArticlesController extends \eapanel\publications\controllers\FrontendControllerAbstract
{
    public function getModelClass() {
        return 'eapanel\publications\models\Article';
    }
    
    public function getSearchClass(){
        return 'eapanel\publications\models\ArticleSearch';
    }

}
