<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\controllers;

/**
 * Description of HotelsController
 *
 * @author Tartharia
 */
class HotelsController extends FrontendControllerAbstract{
    public function getModelClass() {
        return 'eapanel\publications\models\Hotel';
    }
    
    public function getSearchClass(){
        return 'eapanel\publications\models\HotelSearch';
    }
}
