<?php

namespace eapanel\publications\controllers;

use Yii;
use yii\helpers\Url;
use eapanel\publications\Module;
use eapanel\publications\models\PublicationBase;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PublicationController implements the CRUD actions for PublicationBase model.
 * 
 * @property-read string $modelClass Description
 * @property-read string $searchClass Description
 */
abstract class AdminPublicationControllerAbstract extends Controller
{    
    public $formView = '_default';

    public function init() {
        if(!class_exists($this->modelClass))
        {
            throw new \yii\base\InvalidConfigException(Yii::t('publications', 'Invalid model class name'));
        }
        if(!class_exists($this->searchClass))
        {
            throw new \yii\base\InvalidConfigException(Yii::t('publications', 'Invalid search class name'));
        }
        parent::init();
    }
    
    /**
     * @return string model class name
     */
    abstract public function getModelClass();
    
    /**
     * @return string search model class name
     */
    abstract public function getSearchClass();

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PublicationBase models.
     * @return mixed
     */
    public function actionIndex()
    {
        /* @var $searchModel \eapanel\publications\models\PublicationSearch */
        $searchModel = Yii::createObject($this->searchClass,['scenario'=>'search']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $className = $this->modelClass;
        $nameOfPublicationType = $className::getTypeName();

        return $this->render('@eapanel/publications/views/admin_publication/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nameOfPublicationType'=>$nameOfPublicationType
        ]);
    }

    /**
     * Displays a single PublicationBase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@eapanel/publications/views/admin_publication/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PublicationBase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = Yii::createObject(['class'=>$this->modelClass]);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->setFlash('success', Yii::t('publications', 'Publication was added'));
            return $this->redirect('index');
        } else {
            return $this->render('@eapanel/publications/views/admin_publication/create', [
                'model' => $model,
                'formView'=>  $this->formView,
            ]);
        }
    }

    /**
     * Updates an existing PublicationBase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::to(['index']));
        } else {
            return $this->render('@eapanel/publications/views/admin_publication/update', [
                'model' => $model,
                'formView'=>  $this->formView,
            ]);
        }
    }

    /**
     * Deletes an existing PublicationBase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PublicationBase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \eapanel\publications\models\PublicationBase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PublicationBase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
