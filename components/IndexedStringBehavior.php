<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\publications\components;

use yii\db\BaseActiveRecord;

/**
 * Description of IndexedVarcharBehavior
 *
 * @author Tartharia
 */
class IndexedStringBehavior extends \yii\base\Behavior{
    
    /**
     * @var string the attribute, that used to create the index
     */
    public $indexAttribute;
    
    /**
     * @var string 
     */
    public $attribute;

    public function events() {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE=>'generateSlug',
        ];
    }

    public function generateSlug($event) {
        $this->owner->{$this->indexAttribute} = md5($this->owner{$this->attribute});
        
        $model = clone $this->owner;
        $model->clearErrors();
        $validator = new \yii\validators\UniqueValidator();
        $validator->validateAttribute($model, $this->indexAttribute);
        if($model->hasErrors())
        {
            $this->owner->addError($this->attribute,$model->getFirstError($this->indexAttribute));
        }
    }
    
    /**
     * 
     * @param string $param
     * @return \yii\db\ActiveQuery
     */
    public function findByIndexed($param)
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = $this->owner;
        return $model->find()->where("{$this->indexAttribute}=:attr",['attr'=>  md5($param)]);
    }
}
