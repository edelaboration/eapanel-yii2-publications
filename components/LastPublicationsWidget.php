<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace eapanel\publications\components;

use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use eapanel\publications\models\PublicationBase;

/**
 * Description of LastNewsWidget
 *
 * @author Tartharia
 */
class LastPublicationsWidget extends \yii\base\Widget{
    
    public $itemView = '_lastnews';
    
    public $numberOfDisplayed = 5;
    
    public $publicationType;

    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'news-view'];
    
    /**
     * @var array the HTML attributes for the container of the rendering result of each data model.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * If "tag" is false, it means no container element will be rendered.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $itemOptions = [];
    
    public $layout = "{items}";


    /**
     * @var array additional parameters to be passed to [[itemView]] when it is being rendered.
     * This property is used only when [[itemView]] is a string representing a view name.
     */
    public $viewParams = [];
    
    private $dataProvider;

    public function init() {
        $query = PublicationBase::find()->orderBy('id DESC')->limit($this->numberOfDisplayed);
        $query->andFilterWhere(['type'=>  $this->publicationType]);
        $this->dataProvider = new ArrayDataProvider([
            'allModels'=>$query->all(),
            'pagination'=>false
        ]);
    }
    
    public function run() {
        echo ListView::widget([
            'layout'=>  $this->layout,
            'dataProvider'=>  $this->dataProvider,
            'options'=>  $this->options,
            'itemOptions' => $this->itemOptions,
            'itemView' => $this->itemView instanceof \Closure?$this->itemView:"@eapanel/publications/views/news/{$this->itemView}",
            'viewParams' => $this->viewParams
        ]);
    }
}
